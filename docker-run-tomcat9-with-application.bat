echo off
echo Starting tomcat container, can take approximately 1 minute to complete, please wait...
rem docker run -p 8090:8080 -dit --name t9s-dsb-runtime -v //c/Users/Selvyn/demo/vm_share/tomcat-webapps/dsb.war:/usr/local/tomcat/webapps/dsb.war tomcat9-server
docker run -p 8090:8080 -dit --name t9s-dsb-runtime tomcat9-server
timeout 30 /NOBREAK

docker cp dsb.war t9s-dsb-runtime:/usr/local/tomcat/webapps

echo ..
echo ..
echo tomcat should now be available
echo
